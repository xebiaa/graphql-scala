package graphql

import org.parboiled.scala._

class GraphQLParser extends Parser with TokenParser {

  def Document: Rule1[Map[String, GDefinition]] = rule { zeroOrMore(Ignored) ~ oneOrMore(Definition) ~ zeroOrMore(Ignored) ~ EOI } ~~> (_.toMap)

  def Definition: Rule1[(String, GDefinition)] = rule { zeroOrMore(Ignored) ~ (OperationDefinition | FragmentDefinition) ~ zeroOrMore(Ignored) }

  def OperationDefinition: Rule1[(String, GOperationDefinition)] = rule {
    (OperationType ~ Name ~ optional(VariableDefinitions) ~ optional(Directives) ~ SelectionSet ~ zeroOrMore(Ignored)) ~~> GOperationDefinition.fromFullDefinition |
    SelectionSet ~~> GOperationDefinition.fromSelectionSet
  }

  def OperationType: Rule1[String] = rule { zeroOrMore(Ignored) ~ "query" ~ oneOrMore(Ignored) | zeroOrMore(Ignored) ~ "mutation" ~ zeroOrMore(Ignored) } ~> pushString

  def SelectionSet: Rule1[List[GSelection]] = rule { zeroOrMore(Ignored) ~ "{" ~ oneOrMore(Selection) ~ "}" ~ zeroOrMore(Ignored) }

  def Selection: Rule1[GSelection] = rule { zeroOrMore(Ignored) ~ (Field | FragmentSpread | InlineFragment) ~ zeroOrMore(Ignored) }

  def Field: Rule1[GField] =
    rule { zeroOrMore(Ignored) ~ optional(Alias) ~ Name ~ optional(Arguments) ~ optional(Directives) ~ optional(SelectionSet) ~ zeroOrMore(Ignored) } ~~> GField.fromStack _
  def Alias: Rule1[String] = rule { zeroOrMore(Ignored) ~ Name ~ ":" ~ zeroOrMore(Ignored) }

  def Arguments: Rule1[Map[String, GValue]] = rule { zeroOrMore(Ignored) ~ "(" ~ oneOrMore(Argument) ~ ")" ~ zeroOrMore(Ignored) } ~~> (_.toMap)
  def Argument: Rule1[(String, GValue)] = rule { zeroOrMore(Ignored) ~ Name ~ ":" ~ Value ~ zeroOrMore(Ignored) } ~~> ((name, value) => name -> value)

  def FragmentSpread: Rule1[GFragmentSpread] =
    rule { zeroOrMore(Ignored) ~ "..." ~ FragmentName ~ optional(Directives) ~ zeroOrMore(Ignored) } ~~> GFragmentSpread.fromStack _

  def FragmentDefinition: Rule1[(String, GFragmentDefinition)] =
    rule { zeroOrMore(Ignored) ~ "fragment" ~ FragmentName ~ "on" ~ TypeCondition ~ optional(Directives) ~ SelectionSet ~ zeroOrMore(Ignored)} ~~> GFragmentDefinition.fromStack _

  def FragmentName: Rule1[String] = rule { zeroOrMore(Ignored) ~ !"on" ~ Name ~ zeroOrMore(Ignored) }
  def TypeCondition: Rule1[String] = rule { zeroOrMore(Ignored) ~ NamedType ~ zeroOrMore(Ignored) } ~~> ((t) => t.name)

  def InlineFragment: Rule1[GInlineFragment] = rule { zeroOrMore(Ignored) ~ "..." ~ "on" ~ TypeCondition ~ optional(Directives) ~ SelectionSet ~ zeroOrMore(Ignored) } ~~> GInlineFragment.fromStack _

  def Directives: Rule1[Map[String, GDirective]] = rule { zeroOrMore(Ignored) ~ oneOrMore(Directive) ~ zeroOrMore(Ignored) } ~~> (_.toMap)
  def Directive: Rule1[(String, GDirective)] = rule { zeroOrMore(Ignored) ~ "@" ~ Name ~ optional(Arguments) ~ zeroOrMore(Ignored) } ~~> GDirective.fromStack

  def VariableDefinitions: Rule1[Map[String, GVariableDefinition]] = rule { zeroOrMore(Ignored) ~ "(" ~ oneOrMore(VariableDefinition) ~ ")" ~ zeroOrMore(Ignored) } ~~> (_.toMap)
  def VariableDefinition: Rule1[(String, GVariableDefinition)] = rule { zeroOrMore(Ignored) ~ Variable ~ ":" ~ Type ~ optional(DefaultValue) ~ zeroOrMore(Ignored) } ~~> GVariableDefinition.fromStack
  def DefaultValue: Rule1[GValue] = rule { zeroOrMore(Ignored) ~ "=" ~ Value ~ zeroOrMore(Ignored) }
}

sealed trait GDefinition

case class GFragmentDefinition(name: String, typeCondition: String, directives: Map[String, GDirective], selectionSet: Set[GSelection]) extends GDefinition
object GFragmentDefinition {
  def fromStack(name: String, typeCondition: String, directives: Option[Map[String, GDirective]], selectionSet: List[GSelection]) = {
    name -> GFragmentDefinition(name, typeCondition, directives.getOrElse(Map.empty), selectionSet.toSet)
  }
}

case class GOperationDefinition(
    name: String,
    operationType: String,
    variables: Map[String, GVariableDefinition],
    directives: Map[String, GDirective],
    selection: Set[GSelection]) extends GDefinition
object GOperationDefinition {
  def fromFullDefinition = (
    operationType: String,
    name: String,
    variables: Option[Map[String, GVariableDefinition]],
    directives: Option[Map[String, GDirective]],
    selection: List[GSelection]) => {
      name -> GOperationDefinition(
        name,
        operationType,
        variables.getOrElse(Map.empty),
        directives.getOrElse(Map.empty),
        selection.toSet)
    }

  def fromSelectionSet = (selectionSet: List[GSelection]) => "unnamed" -> GOperationDefinition(
    "unnamed",
    "query",
    Map.empty,
    Map.empty,
    selectionSet.toSet
  )
}

case class GDirective(name: String, arguments: Map[String, GValue] = Map.empty)
object GDirective {
  def fromStack = (name: String, arguments: Option[Map[String, GValue]]) => {
    name -> GDirective(name, arguments.getOrElse(Map.empty))
  }
}

sealed trait GSelection
case class GField(alias: Option[String],
                  name: String,
                  arguments: Map[String, GValue],
                  directives: Map[String, GDirective],
                  selectionSet: Set[GSelection]) extends GSelection
object GField {
  def fromStack(alias: Option[String],
      name: String,
      arguments: Option[Map[String, GValue]],
      directives: Option[Map[String, GDirective]],
      selectionSet: Option[List[GSelection]]) = {
    GField(alias, name, arguments.getOrElse(Map.empty), directives.getOrElse(Map.empty), selectionSet.getOrElse(Set.empty).toSet)
  }
}
case class GInlineFragment(typeCondition: String, directives: Map[String, GDirective], selectionSet: Set[GSelection]) extends GSelection
object GInlineFragment {
  def fromStack(typeCondition: String, directives: Option[Map[String, GDirective]], selectionSet: List[GSelection]) = {
    GInlineFragment(typeCondition, directives.getOrElse(Map.empty), selectionSet.toSet)
  }
}
case class GFragmentSpread (name: String, directives: Map[String, GDirective]) extends GSelection
object GFragmentSpread {
  def fromStack(name: String, directives: Option[Map[String, GDirective]]) = {
    GFragmentSpread(name, directives.getOrElse(Map.empty))
  }
}

case class GDirectivesAndSelectionSet(directives: List[GDirective], selectionSet: List[GSelection])

case class GVariableDefinition(name: String, typ: GType, default: Option[GValue] = None)
object GVariableDefinition {
  def fromStack = (variable: GVariable, typ: GType, default: Option[GValue]) => {
    variable.name -> GVariableDefinition(variable.name, typ, default)
  }
}