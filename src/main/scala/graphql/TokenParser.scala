package graphql

import org.parboiled.scala._

trait TokenParser extends CommonParser { self: Parser =>

  // Helpers
  def pushString = (v: String) => v

  // Punctuator
  def Punctuator = rule { anyOf("!$():=@[]{}") | "..." }

  // Identifier
  def Name: Rule1[String] = rule { ("_" | "A" - "Z" | "a" - "z") ~ zeroOrMore("_" | "0" - "9" | "A" - "Z" | "a" - "z") } ~> pushString

  // IntValue
  def IntValue: Rule1[GInt] = rule { IntegerPart } ~> GInt.fromString
  def IntegerPart = rule { (optional(NegativeSign) ~ Zero ~ !Digit) | (optional(NegativeSign) ~ NonZeroDigit ~ zeroOrMore(Digit)) }
  def NegativeSign = rule { str("-")}
  def Zero = rule { str("0")}
  def Digit = rule { "0" - "9"}
  def NonZeroDigit = rule { !Zero ~ Digit}

  // FloatValue
  def FloatValue: Rule1[GFloat] = rule { (IntegerPart ~ FractionalPart ~ ExponentPart) | (IntegerPart ~ ExponentPart) | (IntegerPart ~ FractionalPart) } ~> GFloat.fromString
  def FractionalPart = rule { "." ~ oneOrMore(Digit) }
  def ExponentPart = rule { ExponentIndicator ~ optional(Sign) ~ oneOrMore(Digit) }
  def ExponentIndicator = rule { "E" | "e" }
  def Sign = rule { PositiveSign | NegativeSign }
  def PositiveSign = rule { str("+")}

  // StringValue
  def StringValue: Rule1[GString] = rule { nTimes(2, DoubleQuotes) | DoubleQuotes ~ oneOrMore(StringCharacter) ~ DoubleQuotes } ~> GString.unquoteAndExpand
  def DoubleQuotes = rule { str("\"") }
  def StringCharacter = rule { !DoubleQuotes ~ !BackSlash ~ !LineTerminator ~ SourceCharacter | BackSlash ~ EscapedUnicode | BackSlash ~ EscapedCharacter }
  def BackSlash = rule { str("\\") }
  def EscapedUnicode = rule { "u" ~  nTimes(4, "0" - "9" | "A" - "F" | "a" - "f") }
  def EscapedCharacter = rule { anyOf(""""\/bfnrt""")}

  // BooleanValue
  def BooleanValue: Rule1[GBoolean] = rule { zeroOrMore(Ignored) ~ "true" ~ push(GBoolean(true)) | "false" ~ zeroOrMore(Ignored) ~ push(GBoolean(false)) }

  // EnumValue
  def EnumValue: Rule1[GEnum] = rule { zeroOrMore(Ignored) ~ !("true" | "false" | "null") ~ Name  ~ zeroOrMore(Ignored) } ~~> GEnum

  // Value
  def Value: Rule1[GValue] = rule { zeroOrMore(Ignored) ~ (Variable | FloatValue | IntValue |  StringValue | BooleanValue | EnumValue | ListValue | ObjectValue) ~ zeroOrMore(Ignored) }

  // ObjectValue
  def ObjectValue: Rule1[GObject] = rule { zeroOrMore(Ignored) ~ ("{" ~ "}" ~ zeroOrMore(Ignored)) ~ push(GObject(Map.empty)) |
    zeroOrMore(Ignored) ~ ("{" ~ oneOrMore(ObjectField) ~ "}" ~ zeroOrMore(Ignored)) ~~> ((inp: List[(String, GValue)]) => GObject.fromObjectFields(inp))}
  def ObjectField: Rule2[String, GValue] = rule { zeroOrMore(Ignored) ~ Name ~ ":" ~ zeroOrMore(Ignored) ~ Value ~ zeroOrMore(Ignored) }

  // Variable
  def Variable: Rule1[GVariable] = rule { zeroOrMore(Ignored) ~ "$" ~ Name ~ zeroOrMore(Ignored) } ~~> GVariable

  // Types
  def Type: Rule1[GType] = rule { zeroOrMore(Ignored) ~ (  NonNullType | ListType | NamedType) ~ zeroOrMore(Ignored) }
  def NamedType: Rule1[GNamedType] = rule {zeroOrMore(Ignored) ~ Name ~ zeroOrMore(Ignored) } ~~> GNamedType
  def ListType: Rule1[GType] = rule { zeroOrMore(Ignored) ~ "[" ~ Type ~ "]" ~ zeroOrMore(Ignored) } ~~> GListType
  def NonNullType: Rule1[GType] = rule { zeroOrMore(Ignored) ~ (NamedType ~ "!") | (ListType ~ "!") ~ zeroOrMore(Ignored) } ~~> GNonNullType

  // ListValue
  def ListValue: Rule1[GList] = rule { zeroOrMore(Ignored) ~ ("[" ~ "]") ~ push(GList(Seq.empty)) ~ zeroOrMore(Ignored) | zeroOrMore(Ignored) ~ "[" ~ oneOrMore(Value) ~ "]" ~ zeroOrMore(Ignored) ~~> GList }
}

sealed trait GValue

case class GInt(value: BigInt) extends GValue
object GInt {
  def fromString(s: String) = GInt(BigInt.apply(s))
}

case class GFloat(value: BigDecimal) extends GValue
object GFloat {
  def fromString(s: String) = GFloat(BigDecimal.apply(s))
}

case class GVariable(name: String) extends GValue
case class GString(value: String) extends GValue
object GString {
  def unquoteAndExpand(s: String) = GString(StringContext.processEscapes(s.substring(1, s.length - 1)))
}

case class GBoolean(value: Boolean) extends GValue
case class GEnum(value: String) extends GValue
case class GList(values: Seq[GValue]) extends GValue

sealed trait GType
case class GNamedType(name: String) extends GType
case class GListType(typ: GType) extends GType
case class GNonNullType(name: GType) extends GType

case class GObject(fields: Map[String, GValue]) extends GValue
object GObject {
  def fromObjectFields(fields: Seq[(String, GValue)]) = GObject(fields.toMap)
}



