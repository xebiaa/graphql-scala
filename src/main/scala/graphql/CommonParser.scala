package graphql

import org.parboiled.scala._

trait CommonParser { self: Parser =>
  def SourceCharacter = rule { ANY }
  def WhiteSpace = rule { anyOf(Array('\u0009', '\u000B', '\u000C', '\u0020', '\u00A0')) }
  def LineTerminator = rule { anyOf(Array('\u000A', '\u000D', '\u2028', '\u2029')) }
  def CommentChar = rule { !LineTerminator ~ SourceCharacter }
  def Comment = rule { "#" ~ zeroOrMore(CommentChar) }
  def Comma = rule { str(",") }
  def Ignored = rule { WhiteSpace | LineTerminator | Comment | Comma }
}
