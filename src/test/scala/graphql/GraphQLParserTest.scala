package graphql

import org.parboiled.scala.parserunners.TracingParseRunner
import org.scalatest.{Matchers, FunSuite}

class GraphQLParserTest extends FunSuite with Matchers with ParboiledTestKit {

  val parser = new GraphQLParser

  /**
   * Arguments test
   */
  test("should parse Arguments") {
    new ResultTest[Map[String, GValue]](parser.Arguments) {
      subject.shouldParse("""( name: "AName", foods: ["Fries", 12] )""") { result =>
        result shouldBe Map(
          "name" -> GString("AName"),
          "foods" -> GList(List(GString("Fries"), GInt(12))))
      }
    }
  }

  /**
   * Directives test
   */
  test("should parse Directives") {
    new ResultTest[Map[String, GDirective]](parser.Directives) {
      subject.shouldParse("@test") { result =>
        result shouldBe Map("test" -> GDirective("test"))
      }

      subject.shouldParse("@test(size: 1)") { result =>
        result shouldBe Map("test" -> GDirective("test", Map("size" -> GInt(1))))
      }

      subject.shouldParse("@test(size: 1, amount: \"bla\")") { result =>
        result shouldBe Map("test" -> GDirective("test", Map("size" -> GInt(1), "amount" -> GString("bla"))))
      }
    }
  }

  /**
   * Fragments test
   */
  test("should parse a FragementDefinition") {
    new ResultTest[(String, GFragmentDefinition)](parser.FragmentDefinition) {
      val fragmentInput =
        """
        |fragment standardProfilePic on User {
        |  profilePic(size: 50)
        |}
      """.stripMargin
      subject.shouldParse(fragmentInput) { result =>
        result shouldBe "standardProfilePic" -> GFragmentDefinition("standardProfilePic", "User", Map.empty, Set(
          GField(None, "profilePic", Map("size" -> GInt(50)), Map.empty, Set.empty)
        ))
      }

      val fragmentSpreadInput =
        """
          |fragment friendFields on User {
          |  id
          |  name
          |  ...standardProfilePic
          |}
        """
          .stripMargin
      subject.shouldParse(fragmentSpreadInput) { result =>
        result shouldBe "friendFields" -> GFragmentDefinition("friendFields", "User", Map.empty, Set(
          GField(None, "id", Map.empty, Map.empty, Set.empty),
          GField(None, "name", Map.empty, Map.empty, Set.empty),
          GFragmentSpread("standardProfilePic", Map.empty)
        ))
      }
    }
  }

  /**
   * VariableDefinitions test
   */
  test("should parse VariableDefinitions") {
    new ResultTest[Map[String, GVariableDefinition]](parser.VariableDefinitions) {
      subject.shouldParse( """($devicePicSize: Int, $blowUp: Boolean = true)""") { result =>
        result should contain only(
          "devicePicSize" -> GVariableDefinition("devicePicSize", GNamedType("Int")),
          "blowUp" -> GVariableDefinition("blowUp", GNamedType("Boolean"), Some(GBoolean(true))))
      }
    }
  }

  /**
   * SelectionSet test
   */
  test("should parse a SelectionSet") {
    val simpleSelectionInput =
      """
        |{
        |  id
        |  firstName
        |  lastName
        |}
      """.stripMargin
    new ResultTest[List[GSelection]](parser.SelectionSet) {
      subject.shouldParse(simpleSelectionInput) { result =>
        result shouldBe List(
          GField(None, "id", Map.empty, Map.empty, Set.empty),
          GField(None, "firstName", Map.empty, Map.empty, Set.empty),
          GField(None, "lastName", Map.empty, Map.empty, Set.empty)
        )
      }
    }
  }

  //  val subject = new GraphQLParser
//
//  test("testArgument") {
//    subject.Argument shouldParse "name:Dirk"
//    subject.Argument shouldParse "name: Dirk "
//    subject.Argument shouldParse " \t name : Dirk  \n"
//  }
//
//  test("testArguments") {
//
//  }
//
//  test("testDocumentParser") {
//    val input = """query variableUsedInFragment($atOtherHomes: Boolean) {
//                  |  dog {
//                  |    ...isHousetrainedFragment
//                  |  }
//                  |}
//                  |
//                  |fragment isHousetrainedFragment on Dog {
//                  |  isHousetrained(atOtherHomes: $atOtherHomes)
//                  |}""".stripMargin
//    parse(TracingParseRunner(subject.Document), input) {
//
//    }
//  }
//
//  test("testFieldParser") {
//    val input = """user(name: "Foo Bar") {
//                  |  name
//                  |  friends {
//                  |    name
//                  |  }
//                  |}"""
//    parse(TracingParseRunner(subject.Field), input) {
//    }
//  }
}
