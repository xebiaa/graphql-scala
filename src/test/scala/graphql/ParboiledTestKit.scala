package graphql

import org.parboiled.scala.parserunners.ReportingParseRunner
import org.parboiled.scala.rules.{Rule0, Rule1}
import org.parboiled.scala.testing.ParboiledTest
import org.scalatest.{Matchers, Suite}

trait ParboiledTestKit extends ParboiledTest { self: Suite with Matchers =>

  case class ResultTest[T](subject: Rule1[T]) extends ParboiledTest {

    def fail(msg: String) = self.fail(msg)
    type Result = T

    implicit class TestableRule1(rule: Rule1[T]) {
      def shouldParse(input: String)(f: T => Unit): Unit = {
        parse(ReportingParseRunner(rule), input) {
          matched shouldBe true
          result.foreach(f)
          if (result.isEmpty) fail("Result should be present")
        }
      }

      def shouldNotParse(input: String): Unit = {
        failParse(ReportingParseRunner(rule), input) {
          matched shouldBe false
        }
      }
    }
  }

  implicit class TestableRule0(rule: Rule0) {
    def shouldParse(input: String): Unit = {
      parse(ReportingParseRunner(rule), input) {
        matched shouldBe true
      }
    }
    def shouldNotParse(input: String): Unit = {
      failParse(ReportingParseRunner(rule), input) {
        matched shouldBe false
      }
    }
  }
}
