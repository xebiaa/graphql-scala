package graphql

import org.parboiled.scala._
import org.scalatest.{Matchers, FunSuite}

class CommonParserTest extends FunSuite with Matchers with ParboiledTestKit { 

  val parser = new CommonParser with Parser

  test("testComma") {
    parser.Comma shouldParse ","
  }

  test("testWhiteSpace") {
    parser.WhiteSpace shouldParse "\u0009"
    parser.WhiteSpace shouldParse "\u000B"
    parser.WhiteSpace shouldParse "\u000C"
    parser.WhiteSpace shouldParse "\u0020"
    parser.WhiteSpace shouldParse "\u00A0"
  }

  test("testLineTerminator") {
    parser.LineTerminator shouldParse "\u000A"
    parser.LineTerminator shouldParse "\u000D"
    parser.LineTerminator shouldParse "\u2028"
    parser.LineTerminator shouldParse "\u2029"
  }

  test("testSourceCharacter") {
    parser.SourceCharacter shouldParse "ø"
  }

  test("testCommentChar") {
    parser.CommentChar shouldParse "#"
    parser.CommentChar shouldNotParse "\u000D"
  }

  test("testComment") {
    parser.Comment shouldParse "# This is a comment line"
  }

  test("testIgnored") {
    parser.Ignored shouldParse "\u000C"
    parser.Ignored shouldParse "\u2028"
    parser.Ignored shouldParse ","
    parser.Ignored shouldParse "# This is a comment line"
    parser.Ignored shouldNotParse "ø"
  }

}
