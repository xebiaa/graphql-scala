package graphql

import org.parboiled.scala.Parser
import org.scalatest.{FunSuite, Matchers}

class TokenParserTest extends FunSuite with Matchers with ParboiledTestKit {

  val parser = new TokenParser with CommonParser with Parser

  /**
   * Type tests
   */
  test("should parser a NamedType") {
    new ResultTest[GType](parser.Type) {
      subject.shouldParse("blah") { result =>
        result shouldBe GNamedType("blah")
      }
    }
  }

  test("should parse a ListType") {
    new ResultTest[GType](parser.Type) {
      subject.shouldParse("[blah ]") { result =>
        result shouldBe GListType(GNamedType("blah"))
      }
    }
  }

  test("should parse a NonNullType") {
    new ResultTest[GType](parser.Type) {
      subject.shouldParse("blah !") { result =>
        result shouldBe GNonNullType(GNamedType("blah"))
      }
    }
  }

  test("should parse a ListType within a ListType") {
    new ResultTest[GType](parser.Type) {
      subject.shouldParse("[ [ blah! ] !]") { result =>
        result shouldBe GListType(GNonNullType(GListType(GNonNullType(GNamedType("blah")))))
      }
    }
  }

  /**
   * BooleanValue tests
   */
  test("should parse a BooleanValue with value true") {
    new ResultTest[GBoolean](parser.BooleanValue) {
      subject.shouldParse( """true""") {
        case GBoolean(v) => v shouldBe true
      }
    }
  }

  test("should parse a BooleanValue with value false") {
    new ResultTest[GBoolean](parser.BooleanValue) {
      subject.shouldParse("""false""") {
        case GBoolean(v) => v shouldBe false
      }
    }
  }

  test("should not parse a BooleanValue with non boolean payload") {
    new ResultTest[GBoolean](parser.BooleanValue) {
      subject shouldNotParse """foo"""
    }
  }

  /**
   * Enum tests
   */
  test("should parser an Enum") {
    new ResultTest[GEnum](parser.EnumValue) {
      subject.shouldParse( """myEnum""") {
        case GEnum(v) => v shouldBe "myEnum"
      }
    }
  }

  test("should parser a Boolean") {
    new ResultTest[GEnum](parser.EnumValue) {
      subject.shouldNotParse( """true""")
      subject.shouldNotParse( """false""")
    }
  }

  test("should parse a String") {
    new ResultTest[GEnum](parser.EnumValue) {
      subject.shouldNotParse( """"foo"""")
    }
  }

  /**
   * Float tests
   */
  test("should parse a Sign") {
    parser.Sign shouldParse "-"
    parser.Sign shouldParse "+"
  }

  test("should parse an ExponentIndicator") {
    parser.ExponentIndicator shouldParse "E"
    parser.ExponentIndicator shouldParse "e"
  }

  test("should parse an ExponentPart") {
    Array("e0", "e12", "e-12", "e+12", "E0", "E12", "E-0", "E-12", "E+0", "E+12") foreach { parser.ExponentPart.shouldParse }
  }

  test("should parse a FractionalPart") {
    Array("0", "1", "01", "12") foreach { parser.FractionalPart.shouldNotParse }
    Array(".0", ".1", ".01", ".12") foreach { parser.FractionalPart.shouldParse }
  }

  test("should parse a FloatValue") {
    new ResultTest[GFloat](parser.FloatValue) {
      Array(
        // IntegerPart FractionalPart
        "-0.012" -> -0.012, "0.012" -> 0.012,
        // IntegerPart ExponentPart
        "-1e50" -> -1e50, "1e50" -> 1e50,
        // IntegerPart FractionalPart ExponentPart
        "-1.30e50" -> -1.30e50, "1.30e50" -> 1.30e50, "1.30e-50" -> 1.30e-50, "1.30e+50" -> 1.30e+50
      ) foreach {
        case (input, result) => subject.shouldParse(input) {
          case GFloat(n) => n shouldBe BigDecimal(result)
        }
      }
    }
  }

  /**
   * Int tests
   */
  test("should parse a NegativeSign") {
    parser.NegativeSign shouldParse "-"
  }

  test("should parse a Digit") {
    0 to 9 foreach {
      parser.Digit shouldParse _.toString
    }
  }

  test("should parse a NonZeroDigit") {
    parser.NonZeroDigit shouldNotParse "0"
    1 to 9 foreach {
      parser.NonZeroDigit shouldParse _.toString
    }
  }

  test("should parse an IntegerPart") {
    Array("-0", "0", "-1", "1", "-11", "11") foreach { parser.IntegerPart.shouldParse }
    Array("-01", "01") foreach { parser.IntegerPart.shouldNotParse }
  }

  test("should parse an IntValue") {
    new ResultTest[GInt](parser.IntValue) {
      Array("-1" -> -1, "0" -> 0, "-1" -> -1, "1" -> 1, "-11" -> -11, "11" -> 11) foreach {
        case (input, result) => subject.shouldParse(input) {
          case GInt(n) => n shouldBe BigInt(result)
        }
      }
      Array("-01", "01") foreach { subject.shouldNotParse }
    }
  }

  /**
   * String tests
   */
  test("should parse a simple String") {
    new ResultTest[GString](parser.StringValue) {
      subject.shouldParse(""""Lorem ipsum dollar sit amet"""") {
        case GString(v) => v shouldBe "Lorem ipsum dollar sit amet"
      }
    }
  }

  test("should parse a multiLine String") {
    new ResultTest[GString](parser.StringValue) {
      subject.shouldParse(""""Lorem ipsum\ndollar sit amet"""") {
        case GString(v) => v shouldBe "Lorem ipsum\ndollar sit amet"
      }
    }
  }

  test("should parse a complex String") {
    new ResultTest[GString](parser.StringValue) {
      subject.shouldParse(""""Lorem ipsum døllar sit amet \t :-)! \n &^*$#@"""") {
        case GString(v) => v shouldBe "Lorem ipsum døllar sit amet \t :-)! \n &^*$#@"
      }
    }
  }

  /*
   * List tests
   */
  test("should parse an empty List") {
    new ResultTest[GList](parser.ListValue) {
      Array("[]", " [] ") foreach {
        subject.shouldParse(_) {
          case GList(v) => v shouldBe List.empty
        }
      }
      subject shouldNotParse "[ ]"
    }
  }

  test("should parse a List with entries") {
    new ResultTest[GList](parser.ListValue) {
      Array("[1 2]", "[1, 2]", "[1,, 2]", "[1 , 2]", "[1, 2] ") foreach {
        subject.shouldParse
      }
    }
  }

  /**
   * Object tests
   */
  test("should parse an ObjectValue") {
    new ResultTest[GObject](parser.ObjectValue) {

      subject.shouldParse("{}") {
        case GObject(fields) => fields shouldBe empty
      }

      val input = """{
                    |  intField: 4
                    |  stringField: "Mine",
                    |  floatField: 5.065e12
                    |  enumField: ThisIsEnum
                    |  boolField: true,,
                    |
                    |  listField: ["One" "Two" "Three"   ]
                    |  objectField: {
                    |    intField: 4
                    |    boolField: false
                    |  }
                    |}""".stripMargin
      subject.shouldParse(input) {
        case obj: GObject =>
          obj.fields.keys should contain only("intField", "stringField", "floatField", "enumField", "boolField", "listField", "objectField")
          obj.fields("intField") shouldBe GInt(4)
          obj.fields("stringField") shouldBe GString("Mine")
          obj.fields("floatField") shouldBe GFloat(5.065e12)
          obj.fields("enumField") shouldBe GEnum("ThisIsEnum")
          obj.fields("boolField") shouldBe GBoolean(true)
          obj.fields("listField") shouldBe GList(List(GString("One"), GString("Two"), GString("Three")))
          obj.fields("objectField").asInstanceOf[GObject].fields should contain only("intField" -> GInt(4), "boolField" -> GBoolean(false))
      }
    }
  }

  /**
   * Variable tests
   */
  test("should parse a Variable") {
    new ResultTest[GVariable](parser.Variable) {
      subject.shouldParse("$myVar") {
        case GVariable(v) => v shouldBe "myVar"
      }
    }
  }

  test("a variable should not parse a boolean value") {
    new ResultTest[GVariable](parser.Variable) {
      subject.shouldNotParse("true")
      subject.shouldNotParse("false")
    }
  }

  test("a variable should not parse a name") {
    new ResultTest[GVariable](parser.Variable) {
      subject.shouldNotParse("foo")
    }
  }

  test("a variable should not parse a string value") {
    new ResultTest[GVariable](parser.Variable) {
      subject.shouldNotParse( """"foo"""")
    }
  }
}
