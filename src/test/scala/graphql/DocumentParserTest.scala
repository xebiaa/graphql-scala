package graphql

import org.scalatest.{Matchers, FunSuite}

class DocumentParserTest extends FunSuite with Matchers with ParboiledTestKit {

  type Result = Map[String, GDefinition]
  val subject = (new GraphQLParser).Document

  test("testDocument") {
    val input =
      """
        |query user(id: "1") {
        |  name
        |  friends {
        |    name
        |  }
        |}
        |
        |mutation updateUser($userId: String! $name: String!) {
        |  updateUser(id: $userId name: $name) {
        |    name
        |  }
        |}
      """.stripMargin

//    subject.shouldParse(input) { result =>
//      result shouldBe Map(
//        "user" -> GOperationDefinition(
//          name = "user",
//          operationType = "query",
//          variables = Map.empty,
//          directives = Map.empty,
//          Set()
//        "updateUser" ->
//      )
//    }
  }

}
