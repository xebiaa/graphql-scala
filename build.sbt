name := "scala-graphql"

version := "0.1"

scalaVersion := "2.11.6"

val parboiled = "org.parboiled" %% "parboiled-scala" % "1.1.7"
val scalaTest = "org.scalatest" %%  "scalatest"      % "2.2.4"

libraryDependencies ++= Seq(
  parboiled,
  scalaTest % "test"
)

